import { Link } from 'react-router-dom';


function HatList(props) {
    return (
        <div className="container">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
                <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat!</Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style </th>
                        <th>Color</th>
                        <th>Closet</th>
                        <th>Section</th>
                        <th>Shelf</th>
                        <th>Picture</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats.map(hat => {
                        console.log(hat);
                        return (
                            <>
                                <tr key={hat.href}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style}</td>
                                    <td>{hat.color}</td>
                                    <td>{hat.location.closet_name}</td>
                                    <td>{hat.location.section_number}</td>
                                    <td>{hat.location.shelf_number}</td>

                                    <td><img style={{ width: 50 }} src={hat.picture} alt="" /></td>
                                    <td><button type="button" className="btn btn-danger" onClick={() => deleteItem(`${hat.href}`)}>delete</button> </td>
                                </tr>
                            </>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

async function deleteItem(hatid) {
    const hatUrl = `http://localhost:8090${hatid}`;
    const fetchOptions = {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    await fetch(hatUrl, fetchOptions);
    window.location.reload(true);
}



export default HatList;
