import React from "react";

class CreateShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            picture: '',
            bins: [],
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleManufacturer = this.handleManufacturer.bind(this);
        this.handleModelName = this.handleModelName.bind(this);
        this.handleColor = this.handleColor.bind(this);
        this.handlePicture = this.handlePicture.bind(this);
        this.handleBin = this.handleBin.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.model_name = data.modelName;
        delete data.modelName;
        delete data.bins;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {
            this.setState({
                manufacturer: '',
                modelName: '',
                color: '',
                picture: '',
            })
        }
    }

    handleManufacturer(event) {
        const value = event.target.value;
        this.setState({manufacturer: value });
    }

    handleModelName(event) {
        const value = event.target.value;
        this.setState({modelName: value });
    }

    handleColor(event) {
        const value = event.target.value;
        this.setState({color: value });
    }

    handlePicture(event) {
        const value = event.target.value;
        this.setState({picture: value });
    }

    handleBin(event) {
        const value = event.target.value;
        this.setState({bin: value });
    }

    render() {
        let spinnerClasses = 'd-flex justify-content-center mb-3';
        let dropdownClasses = 'form-select d-none';
        if (this.state.bins.length > 0) {
            spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
            dropdownClasses = 'form-select';
        }

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            messageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
        }

        return (
            <div className="my-5 container">
                <div className="row">
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form className={formClasses} onSubmit={this.handleSubmit} id="create-shoe-form">
                                    <h1 className="card-title">Enter Shoe</h1>
                                    <p className="mb-3">
                                        Please add a shoe
                                    </p>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={this.handleManufacturer} required placeholder="Shoe Manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                            <label htmlFor="manufacturer">Shoe Manufacturer</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={this.handleModelName} required placeholder="Shoe Model" type="text" id="model_name" name="model_name" className="form-control" />
                                            <label htmlFor="manufacturer">Shoe Model</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={this.handleColor} required placeholder="Shoe Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Shoe Color</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={this.handlePicture} required placeholder="Shoe Picture URL" type="url" id="picture" name="picture" className="form-control" />
                                            <label htmlFor="picture">Shoe Picture</label>
                                        </div>
                                    </div>
                                    <div className={spinnerClasses} id="loading-conference-spinner">
                                        <div className="spinner-grow text-secondary" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={this.handleBin} value={this.state.bin} required name="bin" id="bin" className={dropdownClasses}>
                                        <option value="">Choose a bin</option>
                                            {this.state.bins.map(bin => {
                                                return (
                                                    <option key={bin.href} value={bin.href}>
                                                        {bin.closet_name} - {bin.bin_number}
                                                    </option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Add Shoe</button>
                                </form>
                                <div className={messageClasses} id="success-message">
                                    Shoe has been added!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateShoeForm;
