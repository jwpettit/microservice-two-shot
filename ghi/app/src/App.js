import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './Hats';
import HatForm from './HatForm';
import ShoesList from './ShoesList';
import CreateShoeForm from './ShoeForm';

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }

  return (
    <React.Fragment>
      <BrowserRouter>
        <Nav />
        {/* <div className="container"> */}
        <Routes>
          <Route index element={<MainPage />} />

          <Route path="hats" >
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>

          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<CreateShoeForm />} />
          </Route>
        </Routes>
        {/* </div> */}
      </BrowserRouter>
    </React.Fragment>
  );
}


export default App;

