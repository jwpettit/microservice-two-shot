import React from 'react';
import { Link } from 'react-router-dom'

function ShoesList(props) {
    return (
        <>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
                <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Shoe!</Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td><img style={{ width: 50 }} src={shoe.picture} alt="" /></td>
                                <td>{shoe.bin.closet_name} - {shoe.bin.bin_number}</td>
                                <td><button type="button" className="btn btn-danger" onClick={() => deleteItem(`${shoe.href}`)}>DELETE</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

async function deleteItem(shoeid) {
    const shoeUrl = `http://localhost:8080${shoeid}`;
    const fetchOptions = {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    await fetch(shoeUrl, fetchOptions);
    window.location.reload(true);
}

export default ShoesList;
