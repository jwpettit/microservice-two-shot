# Wardrobify

Team:

* Person 2 - Zoybh Hussein- Hats
* Person 1 - Jeff - Shoes

## Design

## Shoes microservice

The shoes microservice consists of an api which tracks a series of attributes related to shoes. When the user clicks on shoes, these attributes are all displayed in a table, per shoe. These attributes are all defined within the model for shoes. The user also has the option from this page to click delete on any shoe, to remove that shoe from their collection.

The wardrobe microservice contains information related to bins where the shoes will be stored. A poller, running on the shoes microservice, polls the wardrobe microservice api for information about the bins and stores them as value objects within the shoes microservice.

From the shoes page, which displays the list of shoes, the user has the option to click the button to add a shoe. This routes them to a page where the user is presented with the option to add shoes, via a form. The form takes in text entered attributes related to the shoe and also has them select, from a dropdown menu, the bin where the shoe will be stored.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Imma try to do it.

UPDATE: I DID IT
