from django.db import models
from django.urls import reverse


# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, blank=True)
    

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField(null=True)
    location = models.ForeignKey( 
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True, 
        blank=True
    )






    def __str__(self):
        return f"{self.color} {self.style}"

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
