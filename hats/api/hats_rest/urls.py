from django.urls import path
from .views import api_list_hats, api_show_hat, api_list_location_vo

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
    path("locations/", api_list_location_vo, name="api_list_location_vo"),
]